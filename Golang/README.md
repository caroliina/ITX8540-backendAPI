# Instructions to deploy Save-API

* go to project folder and deploy using that readme.   

##Alternatively to build locally(Other services would also be needed to work correctly):

1. Install <b>Go</b> (https://golang.org/doc/install)  

2. Install and start <b>MongoDB</b> (https://docs.mongodb.com/manual/administration/install-community/)  

3. Download a MongoDB administrative tool, <b>Robomongo</b> for example (https://robomongo.org/)  

4. Connect to MongoDB with Robomongo  

5. Download <b>mgo</b> package which is a MongoDB driver for Go (https://labix.org/mgo)  

    ```go get gopkg.in/mgo.v2```  

6. Run save-api.go from the command line  

    ```go run save-api.go```  