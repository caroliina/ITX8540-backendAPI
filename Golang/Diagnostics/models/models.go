package models

import (
	"gopkg.in/mgo.v2/bson"
)

type Test struct {
	Id        bson.ObjectId `bson:"_id,omitempty" json:"id"`
	Time      string        `json:"time"`
	PatientId string        `json:"patientId"`
	Type      string        `json:"type"`
	Hand      string        `json:"hand"`
	Results   []string      `json:"results"`
	Data      [][]struct {
		Altang float64 `json:"altang"`
		Aziang float64 `json:"aziang"`
		Force  float64 `json:"force"`
		Time   float64 `json:"time"`
		X      float64 `json:"x"`
		Y      float64 `json:"y"`
	} `json:"data"`
}
type Patient struct {
	Id          string `json:"id"`
	DateOfBirth string `json:"dateOfBirth"`
	Sex         string `json:"sex"`
	Hand        string `json:"hand"`
}
type DescriptiveStatistics struct {
	Id                  bson.ObjectId `bson:"_id,omitempty" json:"id"`
	Averages            []float64     `json:"averages"`
	Labels              []string      `json:"labels"`
	Normalized_averages []float64     `json:"normalized_averages"`
	Normalized_stdevs   []float64     `json:"normalized_stdevs"`
	Normalized_values   []float64     `json:"normalized_values"`
	Stdevs              []float64     `json:"stdevs"`
	Values              []float64     `json:"values"`
}
