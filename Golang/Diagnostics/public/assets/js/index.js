/*
Table filtering
*/
$(document).ready(function () {
    $('.filterable .btn-filter').click(function () {
        var $panel = $(this).parents('.filterable'),
            $filters = $panel.find('.filters input'),
            $tbody = $panel.find('.table tbody');
        if ($filters.prop('disabled') == true) {
            $filters.prop('disabled', false);
            $filters.first().focus();
        } else {
            $filters.val('').prop('disabled', true);
            $tbody.find('.no-result').remove();
            $tbody.find('tr').show();
        }
    });

    $('.filterable .filters input').keyup(function (e) {
        /* Ignore tab key */
        var code = e.keyCode || e.which;
        if (code == '9') return;
        /* Useful DOM data and selectors */
        var $input = $(this),
            inputContent = $input.val().toLowerCase(),
            $panel = $input.parents('.filterable'),
            column = $panel.find('.filters th').index($input.parents('th')),
            $table = $panel.find('.table'),
            $rows = $table.find('tbody tr');
        /* Dirtiest filter function ever ;) */
        var $filteredRows = $rows.filter(function () {
            var value = $(this).find('td').eq(column).text().toLowerCase();
            return value.indexOf(inputContent) === -1;
        });
        /* Clean previous no-result if exist */
        $table.find('tbody .no-result').remove();
        /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
        $rows.show();
        $filteredRows.hide();
        /* Prepend no-result row if all rows are filtered */
        if ($filteredRows.length === $rows.length) {
            $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="' + $table.find('.filters th').length + '">No result found</td></tr>'));
        }
    });
});

/**
CHARTS
**/

/*
5 parameters from current test
5 parameters from nonpatients [avg, std]
*/

// visualizeData([3000.5, 800.0, 3500.0, 1000.2, 3000.7], [[3557.387413610061, 857.5641667763227], [588.2960708753399, 67.57071372983616], [3463.66485143799, 411.645945091818], [3458.1791885217, 890.6390810362], [973.4152483217982, 575.0158357938179]]);

// calcParams: [[avg1, stdev1], [avg2,stdev2], ...]

function visualizeData(curParams, calcParams, labels) {

    var avgParams = $.map(calcParams, function (i) {
        return i[0];
    });
    var stDevUpper = $.map(calcParams, function (i) {
        return parseFloat(i[0]) + parseFloat(i[1]);
    });
    var stDevLower = $.map(calcParams, function (i) {
        return parseFloat(i[0]) - parseFloat(i[1]);
    });

    var data = {
        labels: ["PARAM1", "PARAM2", "PARAM3", "PARAM4", "PARAM5"], // 5 most informative parameters
        datasets: [ // Chosen exercise set, current test set, standard deviation upper and lower
            {
                label: "CURRENT RESULTS",
                backgroundColor: "rgba(200,200,100,0.2)",
                borderColor: "rgba(200,200,100,1)",
                pointBackgroundColor: "rgba(200,200,100,1)",
                pointBorderColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointHoverBorderColor: "rgba(200,200,100,1)",
                data: curParams
            },
            {
                label: "AVERAGE HEALTHY",
                backgroundColor: "rgba(200,200,200,0.2)",
                borderColor: "rgba(200,200,200,1)",
                pointBackgroundColor: "rgba(200,200,200,1)",
                pointBorderColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointHoverBorderColor: "rgba(200,200,200,1)",
                data: avgParams
            },
            {
                label: "STANDARD DEVIATION U",
                backgroundColor: "rgba(255,99,132,0)",
                borderColor: "rgba(255,99,132,1)",
                pointBackgroundColor: "rgba(255,99,132,1)",
                pointBorderColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointHoverBorderColor: "rgba(255,99,132,1)",
                data: stDevUpper
            },
            {
                label: "STANDARD DEVIATION L",
                backgroundColor: "rgba(255,99,132,0)",
                borderColor: "rgba(255,99,132,1)",
                pointBackgroundColor: "rgba(255,99,132,1)",
                pointBorderColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointHoverBorderColor: "rgba(255,99,132,1)",
                data: stDevLower
            }
        ]
    };
    data.labels = labels;

    var options = {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: false,
                    fullWidth: true
                }
                }]
        }
    };

    var ctx = $("#radarChart");
    var myRadarChart = new Chart(ctx, {
        type: 'radar',
        data: data,
        options: options
    });
}

var errors = [];
var radius = 0;

function addErrors(errorData) {
    errors = [];
    $(errorData.data).each(function (index, element) {
        errors.push({
            x: element.center.x,
            y: element.center.y
        });
        radius = element.radius * 1.20;
    });
}

function createDrawing(test) {
    coordinates = [];
    force = [];
    $(test.data).each(function (index, subArray) {
        $(subArray).each(function (index, element) {
            coordinates.push({
                x: element.x,
                y: element.y
            });
            if (element.force > 0) {
                force.push(element.force * 8);
            } else {
                force.push(3);
            }

        });
    });

    $('#drawing').remove();
    $('.graph-container').append('<canvas id="drawing" height="250px" width="600px"></canvas>');
    var ctx = $("#drawing");
    var scatterChart = new Chart(ctx, {
        type: 'line',
        data: {
            datasets: [
                {
                    label: 'Errors',
                    data: errors,
                    backgroundColor: "transparent",
                    pointBackgroundColor: "rgba(255,99,132,0.5)",
                    pointBorderColor: "#fff",
                    radius: radius,
                    borderColor: "transparent"
                },
                {
                    label: 'Points',
                    data: coordinates,
                    backgroundColor: "transparent",
                    pointBackgroundColor: "black",
                    pointBorderColor: "#fff",
                    radius: force
                }
                ]
        },
        options: {
            scales: {
                xAxes: [{
                    type: 'linear',
                    position: 'bottom'
            }]
            }
        }
    });
}