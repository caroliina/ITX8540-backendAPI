$.ajax({
    url: "sample-drawing.json",
    dataType: 'json',
    success: function (json) {
        coordinates = [];
        $(json.data[0]).each(function (index, element) {
            coordinates.push(element);
        });
        draw(coordinates);
    },
    error: function () {
        alert("Error");
    }
});


function draw(coordinates) {
    var path = new Path();
    path.strokeColor = 'black';
    console.log(coordinates.length);
    var start = new Point(coordinates[0].x / 3, coordinates[0].y / 3);
    path.moveTo(start);

    $(coordinates).each(function (i) {
        path.lineTo(coordinates[i].x / 3, coordinates[i].y / 3);
    });
}