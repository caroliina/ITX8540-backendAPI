package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"

	mgo "gopkg.in/mgo.v2"

	"gitlab.com/claantee/ITX8540-backendAPI/Golang/Diagnostics/common"
	"gitlab.com/claantee/ITX8540-backendAPI/Golang/Diagnostics/data"
	"gitlab.com/claantee/ITX8540-backendAPI/Golang/Diagnostics/models"
)

// Handler for HTTP Post - "/patients"
func CreatePatient(w http.ResponseWriter, r *http.Request) {
	var patientData models.Patient

	err := json.NewDecoder(r.Body).Decode(&patientData)
	if err != nil {
		common.DisplayAppError(w, err, "Invalid Patient data", 500)
		return
	}
	patient := &patientData
	context := NewContext()
	defer context.Close()
	c := context.DbCollection("patients")
	// Create patient
	repo := &data.PatientRepository{c}
	err = repo.Create(patient)
	// Create response data
	if err != nil {
		if mgo.IsDup(err) {
			common.DisplayAppError(w, err, "Patient with this Id already exists", http.StatusBadRequest)

			return
		}
	}
	// Send response back
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
}

func GetPatients(w http.ResponseWriter, r *http.Request) {
	// Create new context
	context := NewContext()
	defer context.Close()
	c := context.DbCollection("patients")
	repo := &data.PatientRepository{c}
	// Get all patients
	patients := repo.GetAll()
	// Create response data
	j, err := json.Marshal(patients)
	if err != nil {
		common.DisplayAppError(w, err, "An unexpected error has occurred", 500)
		return
	}
	// Send response back
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func GetPatientsById(w http.ResponseWriter, r *http.Request) {
	// Get id from incoming url
	vars := mux.Vars(r)
	id := vars["id"]

	// create new context
	context := NewContext()
	defer context.Close()
	c := context.DbCollection("patients")
	repo := &data.PatientRepository{c}

	// Get patient by id
	patient, err := repo.GetById(id)
	if err != nil {
		if err == mgo.ErrNotFound {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		common.DisplayAppError(w, err, "An unexpected error has occurred", 500)
		return

	}

	j, err := json.Marshal(patient)
	if err != nil {
		common.DisplayAppError(w, err, "An unexpected error has occurred", 500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func UpdatePatient(w http.ResponseWriter, r *http.Request) {
	// Get id from incoming url
	vars := mux.Vars(r)
	id := vars["id"]
	// Get patientdata
	var patientData models.Patient

	err := json.NewDecoder(r.Body).Decode(&patientData)
	if err != nil {
		common.DisplayAppError(w, err, "Invalid Patient data", 500)
		return
	}
	patient := &patientData

	// create new context
	context := NewContext()
	defer context.Close()
	c := context.DbCollection("patients")
	repo := &data.PatientRepository{c}
	//Update patient by id
	err = repo.Update(patient, id)
	// Get patient by id
	if err != nil {
		if err == mgo.ErrNotFound {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		common.DisplayAppError(w, err, "An unexpected error has occurred", 500)
		return

	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
}
