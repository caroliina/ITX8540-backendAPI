package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	mgo "gopkg.in/mgo.v2"

	"gitlab.com/claantee/ITX8540-backendAPI/Golang/Diagnostics/common"
	"gitlab.com/claantee/ITX8540-backendAPI/Golang/Diagnostics/data"
	"gitlab.com/claantee/ITX8540-backendAPI/Golang/Diagnostics/models"
)

// Handler for HTTP Post - "/tests"
func CreateTest(w http.ResponseWriter, r *http.Request) {
	var dataResource TestResource
	// Decode the incoming Tests json
	err := json.NewDecoder(r.Body).Decode(&dataResource.Data)
	if err != nil {
		common.DisplayAppError(w, err, "Invalid Test data", 500)
		return
	}
	test := &dataResource.Data
	// Create new context
	context := NewContext()
	defer context.Close()
	c := context.DbCollection("tests")
	// Create test
	repo := &data.TestRepository{c}
	repo.Create(test)
	if err != nil {
		common.DisplayAppError(w, err, "An unexpected error has occurred", 500)
		return
	}
	//Get descriptive statistics from DS api
	url := "http://descriptive-api:5000/api/statistics"
	fmt.Println("URL:>", url)

	j, err := json.Marshal(dataResource.Data)
	if err != nil {
		common.DisplayAppError(w, err, "An unexpected error has occurred", 500)
		return
	}
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(j))

	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	//body, _ := ioutil.ReadAll(resp.Body)
	var descriptiveData models.DescriptiveStatistics

	err = json.NewDecoder(resp.Body).Decode(&descriptiveData)
	if err != nil {
		common.DisplayAppError(w, err, "Invalid descriptive data decoding", 500)
		return
	}
	//fmt.Println("response Body:", string(body))
	descriptiveData.Id = test.Id
	dsdata, err := json.Marshal(descriptiveData)
	if err != nil {
		common.DisplayAppError(w, err, "An unexpected error has occurred", 500)
		return
	}
	// Send response back
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(dsdata)
}

func GetTests(w http.ResponseWriter, r *http.Request) {
	// Create new context
	context := NewContext()
	defer context.Close()
	c := context.DbCollection("tests")
	repo := &data.TestRepository{c}
	// Get all tests
	tests := repo.GetAll()
	// Create response data
	j, err := json.Marshal(tests)
	if err != nil {
		common.DisplayAppError(w, err, "An unexpected error has occurred", 500)
		return
	}
	// Send response back
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func GetTestsById(w http.ResponseWriter, r *http.Request) {
	// Get id from incoming url
	vars := mux.Vars(r)
	id := vars["id"]

	// create new context
	context := NewContext()
	defer context.Close()
	c := context.DbCollection("tests")
	repo := &data.TestRepository{c}

	// Get test by id
	test, err := repo.GetById(id)
	if err != nil {
		if err == mgo.ErrNotFound {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		common.DisplayAppError(w, err, "An unexpected error has occurred", 500)
		return

	}

	j, err := json.Marshal(test)
	if err != nil {
		common.DisplayAppError(w, err, "An unexpected error has occurred", 500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func GetTestsByPatientId(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	// Create new context
	context := NewContext()
	defer context.Close()
	c := context.DbCollection("tests")
	repo := &data.TestRepository{c}
	// Get all tests
	tests := repo.GetByPatientId(id)
	// Create response data
	j, err := json.Marshal(tests)
	if err != nil {
		common.DisplayAppError(w, err, "An unexpected error has occurred", 500)
		return
	}
	// Send response back
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func GetDescriptiveStatisticsByTestId(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	id := vars["id"]

	// create new context
	context := NewContext()
	defer context.Close()
	c := context.DbCollection("tests")
	repo := &data.TestRepository{c}

	// Get test by id
	test, err := repo.GetById(id)
	if err != nil {
		if err == mgo.ErrNotFound {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		common.DisplayAppError(w, err, "An unexpected error has occurred", 500)
		return

	}

	//Get descriptive statistics from DS api
	url := "http://descriptive-api:5000/api/statistics"

	j, err := json.Marshal(test)
	if err != nil {
		common.DisplayAppError(w, err, "An unexpected error has occurred", 500)
		return
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(j))

	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	var descriptiveData models.DescriptiveStatistics

	err = json.NewDecoder(resp.Body).Decode(&descriptiveData)
	if err != nil {
		common.DisplayAppError(w, err, "Invalid descriptive data", 500)
		return
	}
	//fmt.Println("response Body:", string(body))
	dsdata, err := json.Marshal(descriptiveData)
	if err != nil {
		common.DisplayAppError(w, err, "An unexpected error has occurred", 500)
		return
	}
	// Send response back
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(dsdata)

}

func GetErrorDetectionByTestId(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	id := vars["id"]

	// create new context
	context := NewContext()
	defer context.Close()
	c := context.DbCollection("tests")
	repo := &data.TestRepository{c}

	// Get test by id
	test, err := repo.GetById(id)
	if err != nil {
		if err == mgo.ErrNotFound {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		common.DisplayAppError(w, err, "An unexpected error has occurred", 500)
		return

	}

	//Get descriptive statistics from DS api
	url := "http://descriptive-api:5000/api/error_detection/"

	j, err := json.Marshal(test)
	if err != nil {
		common.DisplayAppError(w, err, "An unexpected error has occurred", 500)
		return
	}

	//var jsonStr = []byte(`{"title":"Buy cheese and bread for breakfast."}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(j))
	//req.Header.Set("X-Custom-Header", "myvalue")

	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))

	// Send response back
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(body)

}
