package controllers

import (
	"gitlab.com/claantee/ITX8540-backendAPI/Golang/Diagnostics/models"
)

type (
	// For Get - /tests
	TestsResource struct {
		Data []models.Test
	}
	// For Post/Put - /tests
	TestResource struct {
		Data models.Test
	}
)
