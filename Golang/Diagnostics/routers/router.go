package routers

import (
	"github.com/gorilla/mux"
	"gitlab.com/claantee/ITX8540-backendAPI/Golang/Diagnostics/controllers"
)

func InitRoutes() *mux.Router {
	router := mux.NewRouter().StrictSlash(false)
	router.HandleFunc("/tests", controllers.GetTests).Methods("GET")
	router.HandleFunc("/tests/{id}", controllers.GetTestsById).Methods("GET")
	router.HandleFunc("/tests/{id}/ds", controllers.GetDescriptiveStatisticsByTestId).Methods("GET")
	router.HandleFunc("/tests/{id}/error_detection", controllers.GetErrorDetectionByTestId).Methods("GET")
	router.HandleFunc("/tests", controllers.CreateTest).Methods("POST")
	router.HandleFunc("/patients/{id}/tests", controllers.GetTestsByPatientId).Methods("GET")
	router.HandleFunc("/patients", controllers.GetPatients).Methods("GET")
	router.HandleFunc("/patients/{id}", controllers.GetPatientsById).Methods("GET")
	router.HandleFunc("/patients", controllers.CreatePatient).Methods("POST")
	router.HandleFunc("/patients/{id}", controllers.UpdatePatient).Methods("PUT")
	router.PathPrefix("/").HandlerFunc(controllers.StaticHandler)

	return router
}
