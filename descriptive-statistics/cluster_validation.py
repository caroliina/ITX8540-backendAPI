#!/usr/bin/python

import sys
import psycopg2
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.metrics import pairwise_distances
 

# Two different arrays of some data
#X = [
#    [ 5.1,  3.5,  1.4,  0.2],
#    [ 4.9,  3. ,  1.4,  0.2],
#    [ 4.7,  3.2,  1.3,  0.2],
#    [ 4.6,  3.1,  1.5,  0.2],
#    [ 5. ,  3.6,  1.4,  0.2]]

#Y = [
#    [ 15.1,  13.5,  11.4,  10.2],
#    [ 14.9,  13. ,  11.4,  10.2],
#    [ 14.7,  13.2,  11.3,  10.2],
#    [ 14.6,  13.1,  11.5,  10.2],
#    [ 15. ,  13.6,  11.4,  10.2]]

def silhouette_coefficient(X, Y):
    ### SILHOUETTE COEFFICIENT
    # The score is bounded between -1 for incorrect clustering and +1 for highly dense clustering. 
    # Scores around zero indicate overlapping clusters. 
    # The score is higher when clusters are dense and well separated, which relates to a standard concept of a cluster.
    dataset = X + Y
    clusters_nr = 2
    
    ds = pd.DataFrame(dataset)
    ds.columns = ['PD']

    # Group into clusters
    kmeans_model = KMeans(n_clusters=clusters_nr, random_state=1).fit(ds)
    labels = kmeans_model.labels_
    
    # View the results
    # Set the size of the plot
    plt.figure(figsize=(14,7))

    # Create a colormap
    colormap = np.array(['red', 'lime', 'black'])
    
    # Plot the Models Classifications
    plt.subplot(1, 2, 1)
    plt.scatter(ds.PD, ds.PD, c=colormap[labels], s=40)
    plt.title('K Mean Classification')
    plt.show()
    
    silhouette_score = metrics.silhouette_score(ds, labels, metric='euclidean')
    print('Silhouette score', silhouette_score)
    return silhouette_score;

def intra_inter_ratio(X, Y):
    ### INTRACLUSTER TO INTERCLUSTER DISTANCE RATIO
    # Small values of this measure indicate better clustering behaviour.
    intra = pairwise_distances(X, X, metric='euclidean')
    inter = pairwise_distances(X, Y, metric='euclidean')

    size = len(X)
    intra_sum = 0
    inter_sum = 0

    for x in range(0, size):
        intra_sum = intra_sum + sum(intra[x])

    for x in range(0, size):
        inter_sum = inter_sum + sum(inter[x])

    intra_avg = intra_sum / pow(size, 2)
    inter_avg = inter_sum / pow(size, 2)
    
    ratio = intra_avg/inter_avg
    
    print('Intra/Inter ratio', ratio)
    return ratio;
    
#silhouette_coefficient()
#intra_inter_ratio()

def save_cluster_validation(dbname, dbuser, host, port, password):
    # Connecting to the database
    try:
        conn = psycopg2.connect("dbname='" + dbname 
                                + "' user='" + dbuser 
                                + "' host='" + host 
                                + "' port='" + port 
                                + "' password='" + password + "'")
    except:
        print "I am unable to connect to the database..."
    cur = conn.cursor()

    # Select the results from patient and nonpatient tables
    for e in range(5, 6): # Loop through exercise types (should be range(1, 5))
        for p in range(1, 21): # Loop through parameters
            if p == 14:
                continue
            cur.execute("SELECT result from pd_data WHERE exercise_type_id = %s AND parameter_id = %s", (e, p));
            pd_data = []
            for row in cur:
                if row[0] is not None:
                    result = [float(row[0])]
                else:
                    result = [0]
                pd_data.append(result);

            cur.execute("SELECT result from c_data WHERE exercise_type_id = %s AND parameter_id = %s", (e, p));
            c_data = []
            for row in cur:
                if row[0] is not None:
                    result = [float(row[0])]
                else:
                    result = [0]
                c_data.append(result);

            silhouette_coefficient(pd_data, c_data)
            # Insert coefficients and ratios to the database
            """cur.execute("INSERT INTO silhouette_coef (exercise_type_id, parameter_id, result) VALUES (%s,%s,%s)", (e, p, silhouette_coefficient(pd_data, c_data)));
            cur.execute("INSERT INTO distance_ratio (exercise_type_id, parameter_id, result) VALUES (%s,%s,%s)", (e, p, intra_inter_ratio(pd_data, c_data)));
            conn.commit()"""

    conn.close()

        
save_cluster_validation("descriptive_statistics", "postgres", "127.0.0.1", "5432", "***");