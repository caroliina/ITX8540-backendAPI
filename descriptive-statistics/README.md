# Descriptive statistics

## Requirements
1. Python 2
2. PostgreSQL
2. psycopg2
    - [Installation instructions](http://initd.org/psycopg/docs/install.html)
3. SciPy
    - [Installation instructions](http://scipy.org/install.html)
4. scikit-learn
    - [Installation instructions](http://scikit-learn.org/stable/install.html)

## Descriptive statistics
1. descriptive_statistics.py
    - Method ```get_statistics``` takes in the data from a finished test in JSON form.
    - Parameters for the test will be calculated and for every parameter an average (with standard deviation) will be returned 
which is calculated from tests executed by non-patients.
    - Results and normalized results will be returned in a JSON form. 
    - Returned JSON object includes 5 best parameters according to the calculated Silhouette coefficient and intra-inter ratio. 
The more separated the patient and non-patient clusters are the "better" the parameter is.
    - This is used by the API directly after each test has been finished.

## Cluster validation
1. cluster_validation.py
    - Used to calculate the Silhouette coefficients [Silhouette Coefficient](http://scikit-learn.org/stable/modules/generated/sklearn.metrics.silhouette_score.html) and intra-inter ratios for all parameters from the existing tests.
    - Compares the patients and non-patients data - the more distinct the patient and non-patient clusters 
the better it is to use those parameters for identifying abnormalities.
    - Can be called with ```save_cluster_validation(DB_CREDENTIALS)``` but was used only once for calculating the coefficients for existing tests.
Is not being used by the API directly.

## Database
1. descriptive_statistics.sql
    - Includes the SQL scripts to create a DS database and populate the tables with the results from cluster validation

