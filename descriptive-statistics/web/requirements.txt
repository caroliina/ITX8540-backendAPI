matplotlib==1.5.3
numpy==1.11.1
pandas==0.18.1
Flask==0.11.1
scikit_learn==0.18.1
psycopg2
SciPy
