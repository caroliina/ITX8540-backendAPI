import pickle
import os

def obj_save(filename, obj):
    with open(filename, 'wb') as output:
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)

def obj_load(filename, path=''):
    with open(path + filename, 'rb') as input:

        obj = pickle.load(input)
        print(filename + str(type(obj)))

        return obj


def obj_load_multiple(path, ext):
    arr_list = []
    for file in os.listdir(path):
        if file.endswith(ext):
            arr = obj_load(file, path)
            arr_list.append(arr)
    return arr_list
