#!/usr/bin/python

import psycopg2
import json
import numpy
import math
import re
from sklearn import preprocessing

try:
    conn = conn = psycopg2.connect(database="descriptive_statistics", user="postgres", password="docker", host="descriptive_statistics-db", port="5432")
except:
    print ("I am unable to connect to the database...")
    
cur = conn.cursor()

def JK1_TrajAnalysis(coordinates=None):
    dist = [0]
    timeinterval = [0]
    velo = [0]
    acc = [0]
    slope = [0]
    slopedif = [0]
    pressdiff = [0]
    jerk = [0]
    trajLength = 0
    
    for i in range (1 , len(coordinates)):
        dist.insert(i, math.sqrt(pow((coordinates[i, 1] - coordinates[i - 1, 1]), 2) + pow((coordinates[i, 2] - coordinates[i - 1, 2]), 2)))
        trajLength = trajLength + dist[i]
        timeinterval.insert(i, ((coordinates[i, 0] - coordinates[i - 1, 0])))
        velo.insert(i, (dist[i] / timeinterval[i]))
        acc.insert(i, ((velo[i] - velo[i - 1]) / timeinterval[i]))
        slope.insert(i, ((coordinates[i, 2] - coordinates[i - 1, 2]) / (coordinates[i, 1] - coordinates[i - 1, 1])))
        slopedif.insert(i, (slope[i] - slope[i - 1]))
        pressdiff.insert(i, (coordinates[i, 3]))
        jerk.insert(i, ((acc[i] - acc[i - 1]) / timeinterval[i]))
    
    slopedif = transform(slopedif)
    velo = transform(velo)
    pressdiff = transform(pressdiff)
    jerk = transform(jerk)
    
    acc = numpy.array(acc)
    acc = acc[numpy.isfinite(acc)]
    acc_abs = numpy.absolute(acc)
    
    slope = numpy.array(slope)
    slope = slope[numpy.isfinite(slope)]

    timeLength = (coordinates[len(coordinates) - 1, 0] - coordinates[0, 0])
    
    angleMass = numpy.nansum(slopedif) #3022.75202447248
    trajLength #correct
    velocityMass = numpy.nansum(velo) #25386.2843294694
    accMass = numpy.nansum(acc_abs) #628698.498178651
    pressMass = numpy.nansum(pressdiff) #759.378297609997
    timeLength #correct
    jerkMass = numpy.nansum(jerk) #71216208.0058543
    dtw = 1
    angle_time = angleMass/timeLength #74.9170225159236
    traj_time = trajLength/timeLength #correct
    velo_time = velocityMass/timeLength #629.183214272564
    acc_time = accMass/timeLength #15581.8999251178
    press_time = pressMass/timeLength #18.8207172006047
    time_time = 1
    jerk_time = jerkMass/timeLength #1765049.27148444
    dtw_time = dtw/timeLength #629.731598470791
    mean_velo = numpy.mean(velo) #10.9329389877129
    mean_press = numpy.mean(pressdiff) #0.327036303880274
    mean_acc = numpy.mean(acc_abs) #10.4768288304991
    mean_slope = numpy.mean(slope) #0.145143477479205
    
    return [angleMass, trajLength, velocityMass, accMass, pressMass, timeLength, jerkMass, dtw, angle_time, traj_time, velo_time, acc_time, press_time, time_time, jerk_time, dtw_time, mean_velo, mean_press, mean_acc, mean_slope]

def convert_pix_to_mm(pix):
    pix *= (25.4 / 264) * 2
    return pix

def scale_linear_bycolumn(rawpoints, high=1.0, low=0.0):
    mins = numpy.min(rawpoints, axis=0)
    maxs = numpy.max(rawpoints, axis=0)
    rng = maxs - mins
    return high - (((high - low) * (maxs - rawpoints)) / rng)

def transform(array):
    array = numpy.array(array)
    array = array[numpy.isfinite(array)]
    array = numpy.absolute(array)
    
    return array

### Return 20 parameters calculated for the finished test
### [val1, val2, ..., val20]
def calculate_parameters(test_data):
    test_results = numpy.zeros(shape=(1,4))
    
    for i in range (0, len(test_data["data"])):
        for j in range (0, len(test_data["data"][i])):
            x = test_data["data"][i][j]["x"]
            y = test_data["data"][i][j]["y"]
            force = test_data["data"][i][j]["force"]
            time = test_data["data"][i][j]["time"]
            new_row = [time, x, y, force]
            test_results = numpy.vstack([test_results, new_row])
    test_results = numpy.delete(test_results, (0), axis=0)
    parameters_list = JK1_TrajAnalysis(test_results)
    
    return parameters_list
    
### Return top 5 [average, stdev] parameter value pairs from nonpatients data
### [[avg1, stdev1], [avg2, stdev2], ..., [avg5, stdev15]]
def calc_top_parameters(test_type, top_parameters_id, hand_id):
    top_parameters_avg_stdev = []
    for x in range (0, len(top_parameters_id)):
        cur.execute("SELECT AVG(result) FROM c_data_ipad WHERE exercise_type_id = %s AND parameter_id = %s AND hand_id = %s", (test_type, top_parameters_id[x], hand_id));
        average_result = float(cur.fetchone()[0])
        cur.execute("SELECT STDDEV(result) FROM c_data_ipad WHERE exercise_type_id = %s AND parameter_id = %s AND hand_id = %s", (test_type, top_parameters_id[x], hand_id));
        stdev_result = float(cur.fetchone()[0])
        top_parameters_avg_stdev.append([average_result, stdev_result])
    return top_parameters_avg_stdev

### Return a list of top 5 parameter values for current test
### [val1, val2, ..., val5]
def top_current_parameters(parameters_list, top_parameters_id):
    top_current_parameters = []
    for x in range (0, len(top_parameters_id)):
        top_current_parameters.append(parameters_list[top_parameters_id[x] - 1])
    return top_current_parameters
    
### Return a list of top 5 parameter ID's
### [id1, id2, ..., id5]
def top_parameters_id(test_type, hand_id):
    top_parameters = []
    cur.execute("SELECT silhouette_coef_android.parameter_id, parameters.parameter_name FROM silhouette_coef_android JOIN parameters ON silhouette_coef_android.parameter_id = parameters.id WHERE silhouette_coef_android.exercise_type_id = %s AND silhouette_coef_android.hand_id = %s ORDER BY silhouette_coef_android.result DESC LIMIT %s", (test_type, hand_id, 5));
    names = []
    ids = []
    for row in cur:
        ids.append(int(row[0]))
        names.append(row[1])
    return [ids, names]

def create_statistics(test_data):
    # pl_continue, pl_copy, sine_copy, digit, sine_trace
    # 1            2        3          4      5
    # poppelreuter, sine_trace, sin_line_small, pl_trace, pl_copy, pl_continue
    #if (test_data["type"] == "pl_continue" ):
      #  test_type = 1
    #elif (test_data["type"] == "pl_copy" ):
    #    test_type = 2
    if (test_data["type"] == "sine_copy" ):
        test_type = 3
    #elif (test_data["type"] == "digit" ):
     #   test_type = 4
    elif (test_data["type"] == "sine_trace" ):
        test_type = 5
    else:
        test_type = 5

    if (test_data["hand"] == "R" ):
        hand_id = 1
    elif (test_data["hand"] == "L" ):
        hand_id = 2

    parameters_list = calculate_parameters(test_data)
    top_parameters_list = top_parameters_id(test_type, hand_id)
    top_params_names = top_parameters_list[1]
    top_params_ids = top_parameters_list[0]
    top_parameters_avg_stdev = calc_top_parameters(test_type, top_params_ids, hand_id)
    top_current_parameters_list = top_current_parameters(parameters_list, top_params_ids)
    
    normalized_params = []
    normalized_avgs = []
    normalized_stds = []
    for x in range (0, len(top_current_parameters_list)):
        normalized = []
        normalized = scale_linear_bycolumn([top_current_parameters_list[x], top_parameters_avg_stdev[x][0], top_parameters_avg_stdev[x][1]])
        normalized_params.append(normalized[0])
        normalized_avgs.append(normalized[1])
        normalized_stds.append(normalized[2])
    
    return [top_current_parameters_list, top_parameters_avg_stdev, top_params_names, normalized_params, normalized_avgs, normalized_stds]

def stats_to_json(stats):
    data = {}
    data['values'] = stats[0]
    data['labels'] = stats[2]
    avgs = []
    stdevs = []
    for x in range (0, len(stats[1])):
        avgs.append(stats[1][x][0])
        stdevs.append(stats[1][x][1])
    data['averages'] = avgs
    data['stdevs'] = stdevs
    data['normalized_values'] = numpy.array(stats[3]).tolist()
    data['normalized_averages'] = numpy.array(stats[4]).tolist()
    data['normalized_stdevs'] = numpy.array(stats[5]).tolist()
    json_data = json.dumps(data)
    return json_data   

def get_statistics(test_data):
    stats = create_statistics(test_data)
    json_stats = stats_to_json(stats)
    print(json_stats)
    return json_stats

    