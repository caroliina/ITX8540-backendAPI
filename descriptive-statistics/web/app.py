from flask import Flask, jsonify, abort, request, Response
import ds_android
import persistence_manager, build_model

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Flask Dockerized'

@app.route('/api/statistics', methods=['POST'])
def save_json():
     if not request.json:
         abort(400)
 
     try:
         json_stats = ds_android.get_statistics(request.json)
     #     json_stats = ds_android.get_statistics(str(request.json))
     except BaseException as e:
        print(str(e))
        abort(400)
 
     return Response(json_stats, mimetype='application/json')


@app.route('/api/error_detection/', methods=['POST'])
def error_detection_json():
    if not request.json:
        abort(400)

    # Predict
    if True:
        try:
            model_loaded = persistence_manager.obj_load('mdl.pkl', path='./')
             #print("Model loaded")
    
            json_result = model_loaded.predict(request.json)
             #print("Prediction returned")

        except BaseException as e:
            print(str(e))
            abort(400)

    # Test model
    if False:
        try:
            json_request = persistence_manager.obj_load('mdl_test.json', path='./')
            # print('Test JSON loaded')

            model_loaded = persistence_manager.obj_load('mdl.pkl', path='./')
            # print('Model loaded')

            json_result = model_loaded.predict(json_request)
            # print('Prediction returned')

        except BaseException as e:
            return 'Failed: ' + str(e)
            # abort(400)

    return Response(json_result, mimetype='application/json')

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')